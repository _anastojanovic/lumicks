#include "Laser.h"
#include "Laser_controller.h"
#include "Possible_commands.h"
#include <csignal>

void signalHandler(int signum) 
{
	std::cout << "Interrupt signal " << signum << " received.\n";

	exit(signum);
}

bool Possible_commands::sillyMode = false;

int main()
{
	signal(SIGTERM, signalHandler);

	Laser_controller controller;
	Laser laser;
	std::string input;

	while (true)
	{
		std::cout << "Enter command: ";
		getline(std::cin, input);
		controller.sendCommand(laser, input);
		std::cin.clear();
	}

	return 0;
}