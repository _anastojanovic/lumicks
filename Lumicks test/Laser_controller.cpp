#include "Laser_controller.h"

Laser_controller::Laser_controller()
{

}

void Laser_controller::sendCommand(Laser& l, std::string command)
{
	valid = Possible_commands::validCommand(command);

	if (valid)
	{
		l.receiveCommand(*this, command);
	}
	else
	{
		std::cout << "UK!" << std::endl;
	}
}

void Laser_controller::receiveResponse(std::string r)
{
	response = r;
	std::cout << response << std::endl;
}