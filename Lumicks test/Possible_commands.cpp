#include "Possible_commands.h"

bool Possible_commands::validCommand(std::string &s)
{
	if (sillyMode)
	{
		std::string hlp = s;
		for (unsigned int i = 0; i < s.length(); i++)
		{
			s[i] = hlp[s.size() - i - 1];
		}
	}

	if (s == "STR")
		return true;
	else if (s == "STP")
		return true;
	else if (s == "ST?")
		return true;
	else if (s == "KAL")
		return true;
	else if (s == "PW?")
		return true;
	else if (s == "ESM")
	{
		sillyMode = true;
		return true;
	}
	else if (s == "DSM")
	{
		sillyMode = false;
		return true;
	}	

	// This is helper string without a value, because value is not fixed size
	// and there are more options for the value itself
	std::string help = s;
	help.resize(4);
	if (help == "PW=|")
		return true;
	else
		return false;
}