#include "Laser.h"

Laser::Laser()
{
	emitting = false;
	power = 0;
}

void Laser::timer()
{
	std::this_thread::sleep_for(std::chrono::seconds(5));
	emitting = false;
}

void Laser::receiveCommand(Laser_controller &lc, std::string command)
{
	std::string powStr;
	int p;

	if (emitting == false && command == "STR")
	{
		emitting = true;
		response = "STR#";
		//timer();
	}
	else if (emitting == true && command == "STR")
	{
		response = "STR!";
	}

	if (emitting == true && command == "STP")
	{
		emitting = false;
		response = "STP#";
	}
	else if (emitting == false && command == "STP")
	{
		response = "STP!";
	}

	if (command == "ST?")
	{
		if (emitting)
		{
			response = "ST?|1#";
		}
		else
		{
			response = "ST?|0#";
		}
	}

	if (command == "KAL" && emitting == true)
	{
		response = "KAL#";
		//timer();
	}
	else if (command == "KAL" && emitting == false)
	{
		response = "KAL!";
	}

	if (emitting == false && command == "PW?")
	{
		response = "PW?|0";
	}
	else if (emitting == true && command == "PW?")
	{
		std::string s = std::to_string(power);
		response = ("PW?|" + s + "#");
	}

	if (command.substr(0, 4) == "PW=|")
	{
		powStr = command.substr(4);
		p = std::stoi(powStr, nullptr);
	}

	if (emitting == true && command == ("PW=|" + powStr))
	{	
		if (p >= 1 && p <= 100)
		{
			power = p;
			response = "PW=#";
		}
		else
		{
			response = "PW=!";
		}
	}
	else if (emitting == false && command == ("PW=|" + powStr))
	{
		response = "PW=!";
	}

	if (command == "ESM")
	{
		response = "ESM#";
	}

	if (command == "DSM")
	{
		response = "DSM#";
	}

	sendResponse(lc);
}

void Laser::sendResponse(Laser_controller &lc)
{
	lc.receiveResponse(response);
}