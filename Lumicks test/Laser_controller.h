#pragma once
#include <string>
#include <iostream>
#include "Possible_commands.h"
#include "Laser.h"

// This class emulates the laser controller. The function sendCommand() sends the command (if valid) to the laser.
// The function receiveResponse() takes the response from the laser and prints it out to the user

class Laser;

class Laser_controller
{
public:
	Laser_controller();
	void sendCommand(Laser&, std::string);
	void receiveResponse(std::string);

private:
	std::string response;
	bool valid;
};