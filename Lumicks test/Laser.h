#pragma once
#include <string>
#include <thread>
#include <chrono>
#include "Laser_controller.h"

class Laser_controller;

// Class Laser holds the most logic. After it receives the command from the controller, 
// it examinate it for further action. Then the laser sends the response back to the controller.

class Laser
{
public:
	Laser();
	void receiveCommand(Laser_controller&, std::string);
	void sendResponse(Laser_controller&);
	void timer();

private:
	std::string response;
	bool emitting;
	int power;
};